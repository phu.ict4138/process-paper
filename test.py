import pandas as pd

data = pd.read_excel("./data/chinh-tri.xlsx", sheet_name="data")

content = data["content"].tolist()

d = {
    ':': ' ',
    # 'TP.HCM': 'Thành phố Hồ Chí Minh',
    # 'TP': 'Thành phố',
    'Ảnh VGP/Nhật Bắc': ' ',
    'Theo VietNamplus': ' ',
    'Theo Vietnam+': ' ',
    'Theo VGP': ' ',
    'Theo TTXVN': ' ',
    'Theo báo Nhân dân': ' ',
    'Theo baochinhphu': ' ',
    'XEM CLIP': ' ',
    'XEM VIDEO': ' ',
    '\"': ' ',
    '”': ' ',
    '“': ' ',
    ';': ' ',
    '?': ' ',
    '...': ' ',
    '!': ' ',
    '(': ' ',
    ')': ' ',
}

f = open("test.txt", mode="a+", encoding="utf-8")
for i in content:
    j = i.replace('\r', '').replace('\n', ' ')
    j = j.replace('\"', " ")
    j = j.replace(':', " ")
    j = j.replace('”', " ")
    j = j.replace('“', " ")
    j = j.replace(';', " ")
    j = j.replace('...', ' ')
    j = j.replace('XEM VIDEO', ' ')
    j = j.replace('.', '. ')
    j = j.replace('  ', ' ')
    j = j.replace('  ', ' ')
    j = j.replace('  ', ' ')
    j = j.replace('  ', ' ')
    j = j.replace('  ', ' ')
    f.write(j + '\n')

f.close()
print(len(content))
