import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QStackedWidget
from PyQt5.uic import loadUi
from datetime import datetime
import pandas as pd
import webbrowser


class App(QMainWindow):
    def __init__(self):
        super(App, self).__init__()
        loadUi("main.ui", self)
        self.button_next.clicked.connect(self.next)
        self.button_prev.clicked.connect(self.prev)
        self.button_jump.clicked.connect(self.jump)
        self.button_open.clicked.connect(self.open_web)
        self.button_delete.clicked.connect(self.delete_new)
        self.button_save.clicked.connect(self.save_file)

        self.file_input = "chinh-tri.xlsx"
        self.current_index = 0
        self.load_data()
        self.load_paper()
        background = webbrowser.BackgroundBrowser(
            "C://Program Files//Google//Chrome//Application//chrome.exe")
        webbrowser.register('chrome', None, background)

    def load_paper(self):
        current_index = self.current_index
        paper = self.news[current_index]
        current = f"{self.current_index + 1} / {len(self.news)}"
        self.label_number.setText(current)
        self.edit_url.setText(paper["url"])
        self.edit_category.setText(paper["category"])
        self.edit_title.setText(paper["title"])
        self.edit_abstract.setText(paper["abstract"])
        self.edit_content.setText(paper["content"])

    def change(self):
        title = self.edit_title.toPlainText()
        abstract = self.edit_abstract.toPlainText()
        content = self.edit_content.toPlainText()
        current_index = self.current_index
        url = self.news[current_index]["url"]
        self.news[current_index]["title"] = title
        self.news[current_index]["abstract"] = abstract
        self.news[current_index]["content"] = content
        self.data.loc[url]["title"] = title
        self.data.loc[url]["abstract"] = abstract
        self.data.loc[url]["content"] = content

    def next(self):
        if self.current_index == len(self.news) - 1:
            return
        self.change()
        self.current_index += 1
        self.load_paper()
        return

    def prev(self):
        if self.current_index == 0:
            return
        self.change()
        self.current_index -= 1
        self.load_paper()
        return

    def jump(self):
        index = self.edit_jump.toPlainText()
        try:
            index = int(index)
        except:
            index = self.current_index + 1
        if index <= 0:
            index = 1
        if index >= len(self.news):
            index = self.current_index + 1
        if index - 1 != self.current_index:
            self.current_index = index - 1
            self.load_paper()

    def open_web(self):
        current_index = self.current_index
        url = self.news[current_index]["url"]
        webbrowser.get('chrome').open(url)

    def load_data(self):
        filename = "./data/" + self.file_input
        data = pd.read_excel(filename, sheet_name="data")
        data.index = data["url"]
        self.data = data
        self.urls = data["url"].tolist()
        self.news = []
        for i in range(len(self.urls)):
            self.news.append({
                "url": data["url"][i],
                "category": data["category"][i],
                "title": data["title"][i],
                "abstract": data["abstract"][i],
                "content": data["content"][i],
            })
        return

    def delete_new(self):
        current_index = self.current_index
        url = self.news[current_index]["url"]
        self.news.pop(current_index)
        self.data = self.data.drop([url])
        self.load_paper()

    def save_file(self):
        self.change()
        now = datetime.now()
        time = now.strftime("%d_%m_%Y_%H_%M_%S")
        # filename = self.file_input.split(".")[0] + "_" + now + ".xlsx"
        self.data.to_excel(f"./data/{self.file_input}",
                           index=False, sheet_name="data")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setApplicationDisplayName("Một con vịt")
    widget = QStackedWidget()
    main = App()
    widget.addWidget(main)
    widget.setFixedWidth(1700)
    widget.setFixedHeight(900)
    widget.show()
    try:
        sys.exit(app.exec_())
    except:
        print("Closing")
