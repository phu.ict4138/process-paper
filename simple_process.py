import pandas as pd

filename = "chinh-tri.xlsx"

d = {
    ':': ' ',
    'Ảnh VGP/Nhật Bắc': ' ',
    'Theo VietNamplus': ' ',
    'Theo Báo Quân đội Nhân dân': ' ',
    'Theo Vietnam+': ' ',
    'Theo VGP': ' ',
    'Theo TTXVN': ' ',
    'Theo báo Nhân dân': ' ',
    'Theo baochinhphu': ' ',
    'XEM CLIP': ' ',
    'XEM VIDEO': ' ',
    '\"': ' ',
    '\'': ' ',
    '”': ' ',
    '“': ' ',
    ';': ' ',
    '?': ' ',
    '...': ' ',
    '!': ' ',
    '(': ' ',
    ')': ' ',
    '  ': ' ',
    '  ': ' ',
    '  ': ' ',
    '  ': ' ',
    '  ': ' ',
}

data = pd.read_excel(f"./data/{filename}", sheet_name="data")
# data.index = data["url"]
total = data.__len__()
for i in range(total):
    m = data.loc[i]["abstract"]
    n = data.loc[i]["content"]

    m = m.replace('\r', '').replace('\n', ' ')
    for j in d:
        m = m.replace(j, d[j])
    n = n.replace('\r', '').replace('\n', ' ')
    for j in d:
        n = n.replace(j, d[j])
    data.loc[i]["abstract"] = m
    data.loc[i]["content"] = n

data.to_excel(f"./data/{filename}", index=False, sheet_name="data")
